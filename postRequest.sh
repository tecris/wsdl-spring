#!/bin/bash
if hash xmllint 2>/dev/null; then
    curl -s --header "content-type: text/xml" -d @request.xml http://localhost:8080/ws | xmllint --format -
else
    curl -s --header "content-type: text/xml" -d @request.xml http://localhost:8080/ws
fi
echo ''
