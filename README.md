# Web Services with Spring
Boilerplate Spring Web Services

| *Technology*  | *Version* |
| ------------- | ------------- |
| Spring boot | 1.3.2 |
| Java | 8 |
| Wildfly | 10.0.0 |
| Docker | 1.10 |
| Maven | 3.3 |


## How to run application & execute integration tests
### With Docker

**One liner**

 - `$ mvn clean integration-test -Pcontinuous-delivery -Dmaven.buildNumber.doCheck=false`

**Step-by-step**
  ```
  $ docker run -d -p 8080:8080 -p 9990:9990 casadocker/alpine-wildfly-soap:10.0.0    # start wildfly container
  $ mvn clean wildfly:deploy                                                         # deploy application
  $ mvn clean integration-test                                                       # run integration tests
  $ mvn clean wildfly:undeploy                                                       # undeploy application (optional)
  ```

### With Spring Boot
  ```
  $ mvn clean package
  $ java -jar target/ROOT.war
  $ mvn clean integration-test
  ```

### Manual test deployment:
 - `$ ./postRequest.sh`

### WSDL
 - `http://localhost:8080/ws/scoreboard.wsdl`
