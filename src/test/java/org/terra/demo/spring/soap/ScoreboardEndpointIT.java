package org.terra.demo.spring.soap;

import static org.junit.Assert.assertEquals;

import javax.xml.ws.BindingProvider;

import org.junit.Test;
import org.terra.demo.spring.soap.EventType;
import org.terra.demo.spring.soap.ScoreboardRequest;
import org.terra.demo.spring.soap.ScoreboardResponse;
import org.terra.demo.spring.soap.ScoreboardType;
import org.terra.demo.spring.soap.SoccerScoreboardType;

public class ScoreboardEndpointIT {

    private final static String END_POINT_URL = "http://localhost:8080/ws";
    
    @Test
    public void test() {

        
        int homeCorners = 4;
        int visitorsCorners = 8;
        SoccerScoreboardType soccerScoreboardType = new SoccerScoreboardType();
        soccerScoreboardType.setHomeCorners(homeCorners);
        soccerScoreboardType.setVisitorCorners(visitorsCorners);
        
        int homeScore = 2;
        int visitorScore = 3;
        ScoreboardType scoreboardType = new ScoreboardType();
        scoreboardType.setId(0);
        scoreboardType.setSoccerScoreboard(soccerScoreboardType);
        scoreboardType.setHomeScore(homeScore);
        scoreboardType.setVisitorScore(visitorScore);
        
        int gameId = 1;
        int homeTeamId = 2222;
        int visitorTeamId = 4444;
        String competition = "La Tabon";
        String homeTeamName = "Acasa";
        String visitorTeamName = "Vizitator";
        EventType eventType = new EventType();
        eventType.setCompetition(competition);
        eventType.setGameId(gameId);
        eventType.setHomeTeamid(homeTeamId);
        eventType.setHomeTeamName(homeTeamName);
        eventType.setScoreboard(scoreboardType);
        eventType.setVisitorTeamid(visitorTeamId);
        eventType.setVisitorTeamName(visitorTeamName);
        ScoreboardRequest scoreboardRequest = new ScoreboardRequest();
        scoreboardRequest.setEvent(eventType);

        ScoreboardPortService scoreboardPortService = new ScoreboardPortService();
        
        ScoreboardPort scoreboardPort = scoreboardPortService.getScoreboardPortSoap11();
        ((BindingProvider) scoreboardPort).getRequestContext().put(
            BindingProvider.ENDPOINT_ADDRESS_PROPERTY, END_POINT_URL);

        ScoreboardResponse scoreboardResponse = scoreboardPort.scoreboard(scoreboardRequest);
        assertEquals(scoreboardResponse.getEvent().getGameId(), gameId);
    
    }
}
